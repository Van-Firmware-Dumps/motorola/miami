on early-init
    # RAM boost 2.0
    setprop ro.config.moto_swap_supported true
    setprop ro.config.use_common_max_apps true

# common initialization
on post-fs
    # LMK
    setprop ro.lmk.kill_heaviest_task true
    setprop ro.lmk.file_high_percentage 70
    setprop ro.lmk.file_low_percentage 25
    setprop ro.lmk.pgscan_limit 3000
    setprop ro.lmk.swap_free_low_percentage 10
    setprop ro.lmk.swap_util_max 90
    setprop ro.lmk.thrashing_limit 50
    setprop ro.lmk.thrashing_limit_critical 50
    setprop ro.lmk.thrashing_limit_decay 25
    setprop ro.lmk.threshold_decay 50
    setprop ro.lmk.psi_complete_stall_ms 150
    setprop ro.lmk.psi_partial_stall_ms 50
    setprop ro.lmk.stall_limit_medium 1
    setprop ro.lmk.stall_limit_critical 4
    setprop ro.lmk.stall_limit_freeze 8
    setprop ro.lmk.kill_timeout_ms 100

    #LMK 3.1
    setprop ro.lmk.kswapd_limit 90
    setprop ro.lmk.kswapd_limit_decay 10
    setprop ro.lmk.kswapd_min_adj 201
    setprop ro.lmk.use_moto_strategy true
    setprop ro.lmk.medium_min_adj 920
    setprop ro.lmk.critical_min_adj 201
    setprop ro.lmk.freeze_min_adj 0
    setprop ro.lmk.min_thrashing_limit 10

    # Promote recent UI procs
    setprop persist.sys.aitune_promote_recent_ui_procs true
    setprop persist.sys.aitune_promote_important_apps true

    # use psi avg10 for mempressure in fwk to avoid ping-pong.
    setprop ro.config.use_psi_avg10_for_mempressure true

    # delay longer for service restart, will be rescheduled immediately once mempressure backing to normal.
    setprop ro.config.svc_restart_delay_on_moderate_mem 3600000
    setprop ro.config.svc_restart_delay_on_low_mem 3600000
    setprop ro.config.svc_restart_delay_on_critical_mem 3600000

    # App compactor
    setprop ro.config.compact_action_1 4
    setprop ro.config.compact_action_2 2
    setprop ro.config.compact_procstate_throttle 11,18
    #Set default psi for frameowork
    setprop ro.lowmemdetector.psi_low_stall_us 100000
    setprop ro.lowmemdetector.psi_medium_stall_us 150000
    setprop ro.lowmemdetector.psi_high_stall_us 200000
    setprop ro.config.use_freezer true
    # enable qcom perf fw
    setprop persist.sys.perf_fwk_enabled true
    setprop persist.sys.allow_aosp_hints true
    # dex2pro
    setprop persist.sys.dex2pro_enabled true
    setprop persist.sys.dex2pro_art_version 331413030

on property:ro.vendor.hw.ram=4GB
    setprop ro.config.use_compaction true
    setprop ro.config.compact_action_1 4
    setprop ro.config.compact_action_2 2
    setprop ro.config.compact_bootcompleted true
    setprop ro.config.force_procfs_compact true
    setprop ro.lmk.filecache_min_kb 450000
    setprop dalvik.vm.dex2oat-threads 6
    setprop dalvik.vm.heapstartsize 8m
    setprop dalvik.vm.heapgrowthlimit 192m
    setprop dalvik.vm.heapsize 512m
    setprop dalvik.vm.heaptargetutilization 0.75
    setprop dalvik.vm.heapminfree 512k
    setprop dalvik.vm.heapmaxfree 8m
on property:ro.vendor.hw.ram=6GB
    setprop ro.config.use_compaction false
    setprop dalvik.vm.dex2oat-threads 6
    setprop dalvik.vm.heapstartsize 12m
    setprop dalvik.vm.heapgrowthlimit 256m
    setprop dalvik.vm.heapsize 512m
    setprop dalvik.vm.heaptargetutilization 0.5
    setprop dalvik.vm.heapminfree 6m
    setprop dalvik.vm.heapmaxfree 24m
on property:ro.vendor.hw.ram=8GB
    setprop ro.config.use_compaction false
    setprop dalvik.vm.dex2oat-threads 6
    setprop dalvik.vm.heapstartsize 16m
    setprop dalvik.vm.heapgrowthlimit 256m
    setprop dalvik.vm.heapsize 512m
    setprop dalvik.vm.heaptargetutilization 0.5
    setprop dalvik.vm.heapminfree 8m
    setprop dalvik.vm.heapmaxfree 32m

on property:sys.boot_completed=1
    write /dev/cpuset/restricted/cpus 0-3

on init
    mkdir /dev/cpuset/power-daemon
    copy /dev/cpuset/cpus /dev/cpuset/power-daemon/cpus
    copy /dev/cpuset/mems /dev/cpuset/power-daemon/mems
    chown system system /dev/cpuset/power-daemon
    chown system system /dev/cpuset/power-daemon/tasks
    chown system system /dev/cpuset/power-daemon/cgroup.procs
    chmod 0664 /dev/cpuset/power-daemon/tasks
    chmod 0664 /dev/cpuset/power-daemon/cgroup.procs
    write /dev/cpuset/power-daemon/cpus 0-7

# Screen off
on property:debug.tracing.screen_state="1"
    write /dev/cpuset/power-daemon/cpus 0-5

# Screen on
on property:debug.tracing.screen_state="2"
    write /dev/cpuset/power-daemon/cpus 0-7
on property:sys.boot_completed=1 && property:ro.vendor.zram.product_swapon=true
    trigger sys-boot-completed-set

# We want one opportunity per boot to enable zram, so we
# use a trigger we fire from the above stanza. If
# persist.sys.zram_wb_enabled becomes true after boot,
# we swapon with zramwriteback; If
# persist.sys.zram_wb_enabled false, we swapon
# without zramwriteback
on sys-boot-completed-set && property:persist.sys.zram_wb_enabled=true
    swapon_all /vendor/etc/fstab_zramwriteback.qcom

on sys-boot-completed-set && property:persist.sys.zram_wb_enabled=false
    swapon_all /vendor/etc/fstab.qcom

on property:vendor.motosxf.cpusetmode=""
    setprop vendor.motosxf.cpusetmode 0

on property:sys.boot_completed=1 && property:vendor.motosxf.cpusetmode="0"
    write /dev/cpuset/top-app/cpus 0-7
    write /dev/cpuset/foreground/cpus 0-7
    write /dev/cpuset/background/cpus 0-3
    write /dev/cpuset/system-background/cpus 0-5
    write /dev/cpuset/restricted/cpus 0-7

on property:sys.boot_completed=1 && property:vendor.motosxf.cpusetmode="limit_bg1"
    write /dev/cpuset/top-app/cpus 0-7
    write /dev/cpuset/foreground/cpus 0-7
    write /dev/cpuset/system-background/cpus 0-5
    write /dev/cpuset/background/cpus 1-3
    write /dev/cpuset/restricted/cpus 0-7


# close usap Process pool as google suggestion
on property:persist.device_config.runtime_native.usap_pool_enabled=true
    setprop persist.device_config.runtime_native.usap_pool_enabled false

on property:ro.product.cpu.abi=arm64-v8a
    setprop dalvik.vm.dex2oat64.enabled true
    #LHDC
    setprop persist.mot_bt.lhdc_enable true

on property:ro.vendor.hw.ram=4GB
    setprop persist.sys.fw.bservice_age 5000
    setprop persist.sys.fw.bservice_limit 5
    setprop persist.sys.fw.bservice_enable true
    setprop persist.sys.fw.use_trim_settings true
    setprop persist.sys.fw.empty_app_percent 50
    setprop persist.sys.fw.trim_empty_percent 100
    setprop persist.sys.fw.trim_cache_percent 100
    setprop persist.sys.fw.trim_enable_memory 1073741824
on property:ro.vendor.hw.ram=6GB
    setprop persist.sys.fw.bservice_age 5000
    setprop persist.sys.fw.bservice_limit 10
    setprop persist.sys.fw.bservice_enable true
on property:ro.vendor.hw.ram=8GB
    setprop persist.sys.fw.bservice_age 5000
    setprop persist.sys.fw.bservice_limit 15
    setprop persist.sys.fw.bservice_enable true

on property:ro.vendor.hw.frontcolor=*
    setprop ro.boot.hardware.color ${ro.vendor.hw.frontcolor}

